package br.com.builders.prova_api.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AuditedEntity {
	
	@Column(name = "ATUALIZADO_EM")
	private LocalDateTime atualizadoEm;

	public LocalDateTime getAtualizadoEm() {  return atualizadoEm; }

	public void setAtualizadoEm(LocalDateTime atualizadoEm) { this.atualizadoEm = atualizadoEm; }

	@PrePersist
	public void prePersist(){
		atualizadoEm = LocalDateTime.now();
	}

	@PreUpdate
	public void preUpdate(){
		atualizadoEm = LocalDateTime.now();
	}
}
