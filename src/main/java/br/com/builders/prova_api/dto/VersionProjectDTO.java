package br.com.builders.prova_api.dto;

import io.swagger.annotations.ApiModelProperty;

public class VersionProjectDTO {

    @ApiModelProperty(notes = "Nome da aplicação.")
    private String name;

    @ApiModelProperty(notes = "Versão da aplicação.")
    private String version;

    @ApiModelProperty(notes = "Data do build da aplicação.")
    private String buildDate;

    @ApiModelProperty(notes = "Número de revisão da aplicação")
    private int revision;

    public VersionProjectDTO(String name, String version, String buildDate, int revision) {
        this.name = name;
        this.version = version;
        this.buildDate = buildDate;
        this.revision = revision;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}
