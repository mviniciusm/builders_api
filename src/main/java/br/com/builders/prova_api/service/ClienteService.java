package br.com.builders.prova_api.service;

import br.com.builders.prova_api.core.exception.ClienteIDIncorretoException;
import br.com.builders.prova_api.core.exception.ClienteNaoEncontradoException;
import br.com.builders.prova_api.core.exception.CpfInvalidoException;
import br.com.builders.prova_api.core.exception.CpfJaCadastradoException;
import br.com.builders.prova_api.core.exception.EntradaTextoInvalidoException;
import br.com.builders.prova_api.core.utils.CpfValidator;
import br.com.builders.prova_api.core.utils.Validator;
import br.com.builders.prova_api.dto.AtualizarClienteDTO;
import br.com.builders.prova_api.dto.ClienteDTO;
import br.com.builders.prova_api.dto.SalvarClienteDTO;
import br.com.builders.prova_api.model.Cliente;
import br.com.builders.prova_api.repository.ClienteRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClienteService {

    private Logger logger = LoggerFactory.getLogger(ClienteService.class);

    @Autowired
    private ClienteRepository repository;

    public Page<ClienteDTO> obterPorNomeCPF(String nome, String cpf, int pagina, int tamanho) {
        PageRequest pageRequest = PageRequest.of(pagina, tamanho, Sort.Direction.ASC, "cpf");

        return this.repository.obterClientesPaginados( StringUtils.isNotBlank(nome) ? nome.toLowerCase() : null,
                StringUtils.isNotBlank(cpf) ? cpf.toLowerCase() : null,
                pageRequest);
    }

    public ClienteDTO salvar(SalvarClienteDTO dto) {
        Validator.checkArgument(StringUtils.isNotBlank(dto.getNome()), new EntradaTextoInvalidoException());
        Validator.checkArgument(CpfValidator.estaCPFValido(dto.getCpf()), new CpfInvalidoException());
        Optional<Cliente> cliente = this.repository.obterClientePorCPF(dto.getCpf());
        if(cliente.isPresent()) {
            throw new CpfJaCadastradoException();
        }
        return new ClienteDTO(this.repository.save(new Cliente(dto)));
    }

    public void atualizarCliente(AtualizarClienteDTO dto) {
        Validator.checkArgument(dto.getId() == null || dto.getId() > 0, new ClienteIDIncorretoException("ID FORNECIDO INVÁLIDO"));
        Validator.checkArgument(StringUtils.isNotBlank(dto.getNome()), new EntradaTextoInvalidoException());
        Validator.checkArgument(CpfValidator.estaCPFValido(dto.getCpf()), new CpfInvalidoException());

        Optional<Cliente> cliente = this.repository.findById(dto.getId());
        if(cliente.isPresent()) {
            repository.save(cliente.get().obterCliente(dto));
            return;
        }
        throw new ClienteNaoEncontradoException();
    }


    public void atualizarNomeCliente(AtualizarClienteDTO dto) {
        Validator.checkArgument(StringUtils.isNotBlank(dto.getNome()), new EntradaTextoInvalidoException() );
        Optional<Cliente> cliente = this.repository.findById(dto.getId());
        if(cliente.isPresent()) {
            cliente.get().setNome(dto.getNome());
            repository.save(cliente.get());
            return;
        }
        throw new ClienteNaoEncontradoException("Cliente não encontrado!");
    }

    public void removerCliente(Long id) {
        Validator.checkArgument(id == null || id > 0, new ClienteIDIncorretoException("ID FORNECIDO INVÁLIDO"));
        Cliente cliente = this.repository.findById(id).orElseThrow(() -> new ClienteNaoEncontradoException());
        this.repository.deleteById(cliente.getId());
    }

}
