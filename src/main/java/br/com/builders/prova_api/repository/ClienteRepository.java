package br.com.builders.prova_api.repository;

import br.com.builders.prova_api.dto.ClienteDTO;
import br.com.builders.prova_api.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    @Query(" SELECT cliente FROM Cliente cliente " +
            "               WHERE cliente.cpf =:cpf ")
    Optional<Cliente> obterClientePorCPF(   @Param("cpf") String cpf );

    @Query(" SELECT new br.com.builders.prova_api.dto.ClienteDTO(cliente.id, cliente.nome, cliente.cpf, cliente.dataNascimento) FROM Cliente cliente " +
            "               WHERE cliente.id IS NOT NULL " +
            "               AND (:nome is null or lower(cliente.nome) like %:nome%  )   " +
            "               AND (:cpf is null OR cliente.cpf =:cpf)    ")
    Page<ClienteDTO> obterClientesPaginados(  @Param("nome") String nome,
                                              @Param("cpf") String cpf,
                                              Pageable pageable);


}
