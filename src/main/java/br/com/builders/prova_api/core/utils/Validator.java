package br.com.builders.prova_api.core.utils;

import br.com.builders.prova_api.core.exception.GenericoException;
import br.com.builders.prova_api.core.exception.ValidacaoException;
import br.com.builders.prova_api.core.exception.handler.ErrorDetail;

import java.util.ArrayList;
import java.util.List;

public final class Validator {

	private List<ErrorDetail> errors = new ArrayList<>();

	private Validator() {

	}

	public static Validator newInstance() {
		return new Validator();
	}

	public Validator checkArgument(boolean condition, String message, String field) {
		if (!condition) {
			ErrorDetail error = new ErrorDetail(message, field);
			errors.add(error);
		}
		return this;
	}
	
	public static void checkArgument(boolean condition, GenericoException exception) {
		if (!condition) {
			throw exception;
		}
	}

	public void throwExceptions() {
		if (!errors.isEmpty()) {
			throw new ValidacaoException(errors);
		}
	}

	public Validator addErros(List<ErrorDetail> errors) {
		this.errors = errors;
		return this;
	}
}
