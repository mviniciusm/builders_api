package br.com.builders.prova_api.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
    @Profile("!prod")
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.builders.prova_api.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metadata());
    }
    
    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Prova_API")
                .description("Documentação das API's da Prova Builders")
                .version("1.0.0")
                .build();
    }
	
}
