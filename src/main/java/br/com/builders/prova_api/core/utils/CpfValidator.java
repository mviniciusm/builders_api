package br.com.builders.prova_api.core.utils;

public final class CpfValidator {

	private CpfValidator() { }

	private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1; indice >= 0; indice--) {
			int digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	public static boolean estaCPFValido(String cpf) {
		if ((cpf == null) || (cpf.length() != 11))
			return false;
		Integer digito1;
		Integer digito2;
		try {
			digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
			digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		} catch (NumberFormatException ex) {
			return false;
		}
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

}
