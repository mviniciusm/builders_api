package br.com.builders.prova_api.core.utils;

import java.util.ArrayList;
import java.util.List;

public final class MessageResponse {
    private List<String> messages;

    public MessageResponse() {
        super();
    }

    public MessageResponse(List<String> messages) {
        super();
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public static final class Builder {
        private List<String> messages;

        private Builder() {
            messages = new ArrayList<>();
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder messages(List<String> messages) {
            this.messages = messages;
            return this;
        }

        public Builder addMessage(String message) {
            this.messages.add(message);
            return this;
        }

        public MessageResponse build() {
            return new MessageResponse(messages);
        }
    }

}
