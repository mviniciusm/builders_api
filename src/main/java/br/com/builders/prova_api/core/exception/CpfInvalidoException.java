package br.com.builders.prova_api.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "CPF INSERIDO ESTÁ INVÁLIDO")
public class CpfInvalidoException extends GenericoException {

    private static final long serialVersionUID = 1L;

}
