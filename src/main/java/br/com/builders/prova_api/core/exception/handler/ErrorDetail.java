package br.com.builders.prova_api.core.exception.handler;

import java.io.Serializable;

public class ErrorDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;
    private String field;

    public ErrorDetail() {
        super();
    }

    public ErrorDetail(String message) {
        super();
        this.message = message;
        this.field = null;
    }

    public ErrorDetail(String message, String field) {
        super();
        this.message = message;
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

}