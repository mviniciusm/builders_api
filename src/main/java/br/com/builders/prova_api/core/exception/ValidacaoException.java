package br.com.builders.prova_api.core.exception;

import br.com.builders.prova_api.core.exception.handler.ErrorDetail;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class ValidacaoException extends GenericoException {

	private static final long serialVersionUID = 1L;

	private final List<ErrorDetail> errors;

	public ValidacaoException(List<ErrorDetail> errors) {
		this.errors = errors;
	}

	public List<ErrorDetail> getErrors() {
		return errors;
	}
	
}
