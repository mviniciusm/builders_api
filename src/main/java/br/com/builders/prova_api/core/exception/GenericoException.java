package br.com.builders.prova_api.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "ERRO INTERNO NO SERVIDOR")
public class GenericoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public GenericoException() {
        super();
    }

    public GenericoException(Exception e) {
        super(e);
    }

    public GenericoException(String m) {
        super(m);
    }


}
