package br.com.builders.prova_api.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CpfJaCadastradoException extends GenericoException {

    private static final long serialVersionUID = 1L;

    public CpfJaCadastradoException() {  }

    public CpfJaCadastradoException(String message) {
        super(message);
    }
}
