package br.com.builders.prova_api.controller;

import br.com.builders.prova_api.dto.VersionProjectDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/versao")
public class VersionController {

    @ApiOperation(value = "Informações sobre a versão da API.", response = VersionProjectDTO.class)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    VersionProjectDTO versionInfo() {
        return new VersionProjectDTO("Builders_Prova_API", "1.0.0", "", 1);
    }

}
