package br.com.builders.prova_api.controller;

import br.com.builders.prova_api.core.utils.MessageResponse;
import br.com.builders.prova_api.dto.AtualizarClienteDTO;
import br.com.builders.prova_api.dto.ClienteDTO;
import br.com.builders.prova_api.dto.SalvarClienteDTO;
import br.com.builders.prova_api.model.Cliente;
import br.com.builders.prova_api.service.ClienteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @ApiOperation(value = "Obter Cliente",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "GET",
            response = Page.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
    })
    @GetMapping
    public Page<ClienteDTO> obterPorCPF(@RequestParam(required = false) String nome,
                                        @RequestParam(required = false) String cpf,
                                        @RequestParam(required = false) Integer size,
                                        @RequestParam(required = false) Integer page) {
        Pageable pageable = PageRequest.of(page == null ? 0 : page, size == null ? 20 : size);
       return this.clienteService.obterPorNomeCPF(nome, cpf, pageable.getPageNumber(), pageable.getPageSize());
    }

    @ApiOperation(value = "Cadastrar Cliente",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "POST",
            response = ClienteDTO.class)
    @ApiResponses({
                @ApiResponse(code = 200, message = "OK"),
    })
    @PostMapping
    public ClienteDTO salvar(@RequestBody SalvarClienteDTO salvarClienteDTO) {
       return this.clienteService.salvar(salvarClienteDTO);
    }

    @ApiOperation(value = "Atualizar dados do Cliente",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "PUT",
            response = MessageResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
    })
    @PutMapping
    public MessageResponse atualizarCliente(@RequestBody AtualizarClienteDTO atualizarClienteDTO) {
        this.clienteService.atualizarCliente(atualizarClienteDTO);
        return MessageResponse.Builder.newBuilder().addMessage("Dados do cliente atualizados com sucesso").build();
    }

    @ApiOperation(value = "Atualizar nome do cliente",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "PATCH",
            response = MessageResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
    })
    @PatchMapping
    public MessageResponse atualizarNomeCliente(@RequestBody AtualizarClienteDTO atualizarClienteDTO) {
        this.clienteService.atualizarNomeCliente(atualizarClienteDTO);
        return MessageResponse.Builder.newBuilder().addMessage("Nome do cliente atualizado com sucesso").build();
    }

    @ApiOperation(value = "Deletar cliente",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "DELETE",
            response = MessageResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
    })
    @DeleteMapping
    public MessageResponse deletarCliente(@RequestParam Long id) {
        this.clienteService.removerCliente(id);
        return MessageResponse.Builder.newBuilder().addMessage("Cliente removido com sucesso").build();
    }

}
