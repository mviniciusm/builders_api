package br.com.builders.prova_api;

import br.com.builders.prova_api.core.exception.ClienteIDIncorretoException;
import br.com.builders.prova_api.core.exception.ClienteNaoEncontradoException;
import br.com.builders.prova_api.core.exception.CpfInvalidoException;
import br.com.builders.prova_api.core.exception.EntradaTextoInvalidoException;
import br.com.builders.prova_api.dto.AtualizarClienteDTO;
import br.com.builders.prova_api.dto.SalvarClienteDTO;
import br.com.builders.prova_api.model.Cliente;
import br.com.builders.prova_api.repository.ClienteRepository;
import br.com.builders.prova_api.service.ClienteService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ClienteServiceTest {

    @Spy
    @InjectMocks
    private ClienteService service;

    @Mock
    private ClienteRepository repository;

    @Test
    public void obterClientes_deveRetornarListaDeClientes() {
        int page = 0;
        int size = 10;
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "cpf");

        Mockito.when(repository.obterClientesPaginados(null, null, pageRequest))
                .thenReturn(new PageImpl<>(new ArrayList<>()));
        Assert.assertNotNull(this.service.obterPorNomeCPF(null, null, 0, 10));
    }

    @Test
    public void obterClientes_deveRetornarListaDeClientesNaoVazia() {
        int page = 0;
        int size = 10;
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "cpf");

        Mockito.when(repository.obterClientesPaginados(null, null, pageRequest))
                .thenReturn(new PageImpl<>(new ArrayList<>()));
        Assert.assertNotNull(this.service.obterPorNomeCPF(null, null, 0, 10));
    }

    @Test(expected = EntradaTextoInvalidoException.class)
    public void salvarCliente_deveLancarExcecaoNomeInvalidoException() {
        SalvarClienteDTO clienteDTO = new SalvarClienteDTO();
        clienteDTO.setCpf("61931344094");
        clienteDTO.setDataNascimento(LocalDate.now());

        this.service.salvar(clienteDTO);
    }

    @Test(expected = CpfInvalidoException.class)
    public void salvarCliente_deveLancarExcecaoCPFInvalidoException() {
        SalvarClienteDTO clienteDTO = new SalvarClienteDTO();
        clienteDTO.setNome("Rambalac");
        clienteDTO.setDataNascimento(LocalDate.now());
        this.service.salvar(clienteDTO);
    }

    @Test
    @Ignore
    public void salvarCliente_deveSalvarCliente() {
        SalvarClienteDTO clienteDTO = new SalvarClienteDTO();
        clienteDTO.setNome("Rambalac");
        clienteDTO.setCpf("61931344094");
        Mockito.when(repository.obterClientePorCPF("61931344094")).thenReturn(Optional.of(new Cliente()));
        clienteDTO.setDataNascimento(LocalDate.now());
        this.service.salvar(clienteDTO);
    }

    @Test(expected = ClienteNaoEncontradoException.class)
    public void atualizarNomeCliente_deveLancarCLienteNaoEncontradaException() {
        AtualizarClienteDTO clienteDTO = new AtualizarClienteDTO();
        clienteDTO.setCpf("61931344094");
        clienteDTO.setNome("Rambalac");
        clienteDTO.setDataNascimento(LocalDate.now());

       service.atualizarNomeCliente(clienteDTO);

    }

    @Test(expected = EntradaTextoInvalidoException.class)
    public void atualizarNomeCliente_deveLancarExcecaoTextoInvalido() {
        AtualizarClienteDTO clienteDTO = new AtualizarClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setCpf("61931344094");
        clienteDTO.setDataNascimento(LocalDate.now());

        service.atualizarNomeCliente(clienteDTO);

    }

    @Test(expected = CpfInvalidoException.class)
    public void atualizarCliente_deveLancarCPFInvalidoException() {
        AtualizarClienteDTO clienteDTO = new AtualizarClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setCpf("61931344x94");
        clienteDTO.setNome("Rambalac");
        clienteDTO.setDataNascimento(LocalDate.now());

        service.atualizarCliente(clienteDTO);

    }


    @Test(expected = ClienteNaoEncontradoException.class)
    public void atualizarCliente_deveLancarCLienteNaoEncontradaException() {
        AtualizarClienteDTO clienteDTO = new AtualizarClienteDTO();
        clienteDTO.setCpf("61931344094");
        clienteDTO.setNome("Rambalac");
        clienteDTO.setDataNascimento(LocalDate.now());

        service.atualizarNomeCliente(clienteDTO);

    }

    @Test(expected = EntradaTextoInvalidoException.class)
    public void atualizarCliente_deveLancarExcecaoTextoInvalido() {
        AtualizarClienteDTO clienteDTO = new AtualizarClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setCpf("61931344094");
        clienteDTO.setDataNascimento(LocalDate.now());

        service.atualizarNomeCliente(clienteDTO);

    }

    @Test(expected = ClienteIDIncorretoException.class)
    public void removerCliente_deveLancarExcecaoClienteIDInvalido() {
        Long id = -1L;
        service.removerCliente(id);
    }


}
