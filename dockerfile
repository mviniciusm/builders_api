FROM openjdk:latest
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY ./build/libs/* ./app.jar
EXPOSE 8090
CMD ["java","-jar","app.jar"]